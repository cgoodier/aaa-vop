//
//  arrive-alive-ios
//
//  Created by Cole GOODIER on 2/5/19.
//  Copyright © 2019 Cole GOODIER. All rights reserved.
//
//

import Foundation
import UIKit
import Pulley
import LyftSDK
import UberRides


enum service {
    case lyft
    case uber
    case none
}

class InfoSheetViewController: UIViewController, PulleyDrawerViewControllerDelegate {

    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var startupMessage: UILabel!
    @IBOutlet weak var fasterButton: UIButton!
    @IBOutlet weak var cheaperButton: UIButton!
    @IBOutlet weak var bothButton: UIButton!
    @IBOutlet weak var lyftButton: LyftButton!
    @IBOutlet weak var alternativeOptionButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var lyftOptions: UISegmentedControl!
    @IBOutlet weak var uberOptions: UISegmentedControl!

    let uberButton = RideRequestButton()

    var cheaper = service.none
    var faster = service.none
    var lyftRideType = "lyft"
    var uberRideType = "uberx"

    // update the selected ride type for lyft
    @IBAction func changeLyftType(_ sender: Any) {
        if lyftOptions.selectedSegmentIndex == 0 {
            lyftModel.setRideKind("lyft")
            lyftRideType = "lyft"
        }
        else if lyftOptions.selectedSegmentIndex == 1 {
            lyftModel.setRideKind("lyft_plus")
            lyftRideType = "lyft_plus"
        }
        else if lyftOptions.selectedSegmentIndex == 2 {
            lyftModel.setRideKind("lyft_lux")
            lyftRideType = "lyft_lux"
        }
        else if lyftOptions.selectedSegmentIndex == 3 {
            lyftModel.setRideKind("lyft_premier")
            lyftRideType = "lyft_premier"
        }
        if startupMessage.isHidden == false {
            return
        }
        print("Changed Lyft ride type to \(lyftRideType)")
        refresh(self)
    }

    // update the selected ride type for uber
    @IBAction func changeUberType(_ sender: Any) {
        if uberOptions.selectedSegmentIndex == 0 {
            uberModel.setRideKind(rideKind: "uberx")
            uberRideType = "uberx"
        }
        else if uberOptions.selectedSegmentIndex == 1 {
            uberModel.setRideKind(rideKind: "uberxl")
            uberRideType = "uberxl"
        }
        else if uberOptions.selectedSegmentIndex == 2 {
            uberModel.setRideKind(rideKind: "uberblack")
            uberRideType = "uberblack"
        }
        else if uberOptions.selectedSegmentIndex == 3 {
            uberModel.setRideKind(rideKind: "suv")
            uberRideType = "suv"
        }
        if startupMessage.isHidden == false {
            return
        }
        print("Changed Uber ride type to \(uberRideType)")
        refresh(self)
    }

    // return to display of cheaper/faster options from ride call button for a service
    @IBAction func goBack(_ sender: Any) {
        header.isHidden = false
        updateButtons()
    }

    // refresh the lyft/uber buttons and their model data, then update UI buttons
    @IBAction func refresh(_ sender: Any) {
        refreshButton.setTitle("Refreshing...", for: .normal)
        configureServiceButtons()
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            print("Refreshing...")
            self.updateButtons()
            self.refreshButton.setTitle("Refresh", for: .normal)
        })
    }

    // switch ride call button to other service on user button press
    @IBAction func tryAlternative(_ sender: Any) {
        header.isHidden = true
        if lyftButton.isHidden == false {
            lyftButton.isHidden = true
            uberButton.isHidden = false
            alternativeOptionButton.setTitle("Try Lyft Instead", for: .normal)
            alternativeOptionButton.setTitleColor(UIColor.magenta, for: .normal)
            alternativeOptionButton.isHidden = false
            backButton.isHidden = false
        }
        else if uberButton.isHidden == false {
            uberButton.isHidden = true
            lyftButton.isHidden = false
            alternativeOptionButton.setTitle("Try Uber Instead", for: .normal)
            alternativeOptionButton.setTitleColor(UIColor.black, for: .normal)
            alternativeOptionButton.isHidden = false
            backButton.isHidden = false
        }
    }

    // show the ride call button and alternative option when user selects the faster option
    @IBAction func fasterPress(_ sender: Any) {
        header.isHidden = true
        cheaperButton.isHidden = true
        fasterButton.isHidden = true
        if faster == service.lyft {
            lyftButton.isHidden = false
            alternativeOptionButton.setTitle("Try Uber Instead", for: .normal)
            alternativeOptionButton.setTitleColor(UIColor.black, for: .normal)
            alternativeOptionButton.isHidden = false
            backButton.isHidden = false
        }
        else {
            uberButton.isHidden = false
            alternativeOptionButton.setTitle("Try Lyft Instead", for: .normal)
            alternativeOptionButton.setTitleColor(UIColor.magenta, for: .normal)
            alternativeOptionButton.isHidden = false
            backButton.isHidden = false
        }
    }

    // show the ride call button and alternative option when user selects the cheaper option
    @IBAction func cheaperPress(_ sender: Any) {
        header.isHidden = true
        cheaperButton.isHidden = true
        fasterButton.isHidden = true
        if cheaper == service.lyft {
            lyftButton.isHidden = false
            alternativeOptionButton.setTitle("Try Uber Instead", for: .normal)
            alternativeOptionButton.setTitleColor(UIColor.black, for: .normal)
            alternativeOptionButton.isHidden = false
            backButton.isHidden = false
        }
        else {
            uberButton.isHidden = false
            alternativeOptionButton.setTitle("Try Lyft Instead", for: .normal)
            alternativeOptionButton.setTitleColor(UIColor.magenta, for: .normal)
            alternativeOptionButton.isHidden = false
            backButton.isHidden = false
        }
    }

    // show ride call and alternative option buttons on button press for single dominant option
    @IBAction func bothPress(_ sender: Any) {
        header.isHidden = true
        bothButton.isHidden = true
        if bothButton.backgroundColor == UIColor.magenta {
            lyftButton.isHidden = false
            alternativeOptionButton.setTitle("Try Uber Instead", for: .normal)
            alternativeOptionButton.setTitleColor(UIColor.black, for: .normal)
            alternativeOptionButton.isHidden = false
            backButton.isHidden = false
        }
        else if bothButton.backgroundColor == UIColor.black {
            uberButton.isHidden = false
            alternativeOptionButton.setTitle("Try Lyft Instead", for: .normal)
            alternativeOptionButton.setTitleColor(UIColor.magenta, for: .normal)
            alternativeOptionButton.isHidden = false
            backButton.isHidden = false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // call to disable minimized drawer position
        if let drawer = self.parent as? PulleyViewController {
            drawer.initialDrawerPosition = PulleyPosition.partiallyRevealed
            drawer.setNeedsSupportedDrawerPositionsUpdate()
        }

        // adjust UI buttons
        header.isHidden = true
        startupMessage.isHidden = false
        lyftButton.isHidden = true
        uberButton.isHidden = true
        cheaperButton.isHidden = true
        cheaperButton.layer.cornerRadius = 10
        cheaperButton.layer.masksToBounds = true
        fasterButton.isHidden = true
        fasterButton.layer.cornerRadius = 10
        fasterButton.layer.masksToBounds = true
        bothButton.isHidden = true
        bothButton.layer.cornerRadius = 10
        bothButton.layer.masksToBounds = true
        alternativeOptionButton.isHidden = true
        backButton.isHidden = true
        refreshButton.isHidden = true
        refreshButton.layer.cornerRadius = 10
        refreshButton.layer.masksToBounds = true

        // programmatically add uber button and its constraints
        view.addSubview(uberButton)
        uberButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: uberButton, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lyftButton, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: uberButton, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lyftButton, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: uberButton, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lyftButton, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: uberButton, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lyftButton, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1.0, constant: 0.0).isActive = true
        NSLayoutConstraint(item: uberButton, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: lyftButton, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1.0, constant: 0.0).isActive = true

        // set initial ride types
        lyftModel.setRideKind(lyftRideType)
        uberModel.setRideKind(rideKind: uberRideType)
    }

    // override to disable the minimized drawer position
    func supportedDrawerPositions() -> [PulleyPosition] {
        return [PulleyPosition.partiallyRevealed, PulleyPosition.open]
        // return [PulleyPosition.partiallyRevealed]
    }

    // hide startup message after user initially enters valid locations
    func removeStartupMessage() {
        startupMessage.isHidden = true
        header.text = "Show me what's..."
        header.isHidden = false
        refreshButton.isHidden = false
    }

    // hide all UI buttons aside from Go and Refresh
    func hideButtons() {
        lyftButton.isHidden = true
        uberButton.isHidden = true
        cheaperButton.isHidden = true
        fasterButton.isHidden = true
        bothButton.isHidden = true
        alternativeOptionButton.isHidden = true
        backButton.isHidden = true
    }

    // update call ride buttons with currently set parameters
    func configureServiceButtons() {
        lyftModel.makeButton(lyftButton: lyftButton, rideKind: lyftRideType)
        uberModel.makeButton(uberButton: uberButton, productGroup: uberRideType)
    }

    // update the displayed options and buttons within the drawer
    func updateButtons() {
        // remove UI elements from previous use
        hideButtons()

        // neither service is available
        guard uberModel.cost != nil else {
            guard lyftModel.minCost != nil else {
                header.text = "Sorry, no rides available."
                header.isHidden = false
                startupMessage.text = "Try adjusting your destination or pickup location."
                startupMessage.isHidden = false
                return
            }
            print("Uber Model Cost or ETA could not be unwrapped")
            header.text = "Uber not available. Try..."
            header.isHidden = false
            lyftButton.isHidden = false
            refreshButton.isHidden = false
            return
        }

        // lyft is not available
        guard let lyftCost = lyftModel.minCost, let lyftTime = lyftModel.driverETA else {
            print("Lyft Model Cost or ETA could not be unwrapped")
            header.text = "Lyft not available. Try..."
            header.isHidden = false
            uberButton.isHidden = false
            refreshButton.isHidden = false
            return
        }

        // uber is not available
        guard let uberCost = uberModel.cost, let uberTime = uberModel.driverETA else {
            print("Uber Model Cost or ETA could not be unwrapped")
            header.text = "Uber not available. Try..."
            header.isHidden = false
            lyftButton.isHidden = false
            refreshButton.isHidden = false
            return
        }

        // comparisons
        if lyftCost < Decimal(uberCost) {
            cheaper = service.lyft
        }
        else if lyftCost > Decimal(uberCost) {
            cheaper = service.uber
        }
        else {
            cheaper = service.none
        }

        if lyftTime < uberTime {
            faster = service.lyft
        }
        else if lyftTime > uberTime {
            faster = service.uber
        }
        else {
            faster = service.none
        }

        removeStartupMessage()

        // one service is both cheaper and faster, show one button
        if cheaper == faster && cheaper != service.none {
            if cheaper == service.lyft {
                bothButton.backgroundColor = UIColor.magenta
                bothButton.setTitle("Cheaper & Faster!", for: .normal)
            }
            else if cheaper == service.uber {
                bothButton.backgroundColor = UIColor.black
                bothButton.setTitle("Cheaper & Faster!", for: .normal)
            }
            bothButton.isHidden = false
        }

        // there is a tie for at least one category
        else if cheaper == service.none || faster == service.none {
            // tie for both categories
            if cheaper == service.none && faster == service.none {
                let minutes = lyftTime/60
                header.text = "Both cost at least $\(lyftCost) and take \(minutes) mins."
                header.isHidden = false
                bothButton.backgroundColor = UIColor.magenta
                bothButton.setTitle("View Lyft", for: .normal)
                bothButton.isHidden = false
            }
            // tie for cheaper
            else if cheaper == service.none {
                header.text = "Both cost at least $\(lyftCost). But what's..."
                if faster == service.lyft {
                    bothButton.backgroundColor = UIColor.magenta
                    bothButton.setTitle("Faster?", for: .normal)
                }
                else if faster == service.uber {
                    bothButton.backgroundColor = UIColor.black
                    bothButton.setTitle("Faster?", for: .normal)
                }
                header.isHidden = false
                bothButton.isHidden = false
            }
            // tie for faster
            else {
                let minutes = lyftTime/60
                if minutes == 1 {
                    header.text = "Both take about \(minutes) minute. But what's..."
                }
                else {
                    header.text = "Both take about \(minutes) mins. But what's..."
                }
                if cheaper == service.lyft {
                    bothButton.backgroundColor = UIColor.magenta
                    bothButton.setTitle("Cheaper?", for: .normal)
                }
                else if cheaper == service.uber {
                    bothButton.backgroundColor = UIColor.black
                    bothButton.setTitle("Cheaper?", for: .normal)
                }
                header.isHidden = false
                bothButton.isHidden = false
            }
        }

        // one service is cheaper and the other is faster
        else {
            if cheaper == service.lyft {
                cheaperButton.backgroundColor = UIColor.magenta
            }
            else if cheaper == service.uber {
                cheaperButton.backgroundColor = UIColor.black
            }
            if faster == service.lyft {
                fasterButton.backgroundColor = UIColor.magenta
            }
            else if faster == service.uber {
                fasterButton.backgroundColor = UIColor.black
            }
            fasterButton.isHidden = false
            cheaperButton.isHidden = false
        }
    }
}
