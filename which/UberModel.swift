//
//  arrive-alive-ios
//
//  Created by Cole GOODIER on 2/5/19.
//  Copyright © 2019 Cole GOODIER. All rights reserved.
//
//

import Foundation
import CoreLocation
import UberCore
import UberRides

class UberModel {
    var pickup: CLLocation?
    var dropoff: CLLocation?
    
    var rideKind: String?
    var productID: String?
    var cost: Int?
    var driverETA: Int?
    let ridesClient = RidesClient()
    var UberButton = RideRequestButton()
    var builder = RideParametersBuilder()
    
    init() {
    }
    
    func setPickupLocation(pickup: CLLocation) {
        self.pickup = Optional.some(pickup)
    }
    
    func setDropoffLocation(dropoff: CLLocation) {
        self.dropoff = Optional.some(dropoff)
    }
    
    // Sets the Uber model's rideKind and productID variables. productID is found based on the chosen rideKind.
    func setRideKind(rideKind: String) {
        guard let pickup = self.pickup else {
            print("Uber Model RideKind ERROR: no pickup location")
            return
        }
        
        self.rideKind = Optional.some(rideKind)
        var product: ProductGroup
        
        // Matched the appropriate productGroup to product based on the rideKind.
        switch rideKind {
        case "uberx":
            product = .uberX
        case "uberxl":
            product = .uberXL
        case "uberblack":
            product = .uberBlack
        case "suv":
            product = .suv
        default:
            product = .unknown
        }
        
        // Makes API call to retireve the product ID. Filters products by productGroup
        self.ridesClient.fetchProducts(pickupLocation: pickup) { products, response in
            // Gets the top result of the list of products(rides) that have the same productGroup as 'product'.
            guard let uber = products.filter({$0.productGroup == product}).first else {
                // Handle error, UberX does not exist at this location
                print("Uber Model RideKind ERROR: No \(rideKind) rides available in this area")
                return
            }
            
            self.setProductID(productID: uber.productID)
        }
    }
    
    // Sets productID and prints error message if it could not be set properly.
    func setProductID(productID: String?) {
        self.productID = productID
        print("Uber Model Product ID: \(self.productID ?? "Uber Model Product ID ERROR: productID not set")")
    }
    
    // Sets Uber model's cost variable for the chosen ride.
    func setCost() {
        guard let pickup = self.pickup else {
            print("Uber Model Cost ERROR: pickup location not set")
            return
        }
        
        guard let dropoff = self.dropoff else {
            print("Uber Model Cost ERROR: dropoff location not set")
            return
        }
        
        guard let productID = self.productID else {
            print("Uber Model Cost ERROR: productID not set")
            return
        }
        
        // API call to get price estimates.
        ridesClient.fetchPriceEstimates(pickupLocation: pickup, dropoffLocation: dropoff, completion:{ priceEstimates, response in
            // Filtered by product ID and gets the top result.
            self.cost = priceEstimates.filter({$0.productID == productID}).first?.lowEstimate
            print("Uber Model Minimum Cost: $\(self.cost?.description ?? "Uber Model Cost ERROR: in API call")")
            })
    }
    
    // Sets driver ETA using API call to fetch time estimates.
    func setDriverETA() {
        guard let pickup = self.pickup else {
            print("Uber Model ETA ERROR: pickup location not set")
            return
        }
        
        guard let productID = self.productID else {
            print("Uber Model ETA Error: productID not set")
            return
        }
        
        // API call to get list of driverETAs.
        ridesClient.fetchTimeEstimates(pickupLocation: pickup, productID: productID,completion:{
            timeEstimates, response in
            // Filtered by product ID and gets the top result.
            self.driverETA = timeEstimates.filter({$0.productID == productID}).first?.estimate
            print("Uber Model Driver ETA: \(self.driverETA?.description ?? "Uber Model ETA ERROR: in API call")")
        })
    }
    
    // Updates Uber model's variables for ride kind, cost, and driver ETA.
    func updateAll() {
        setCost()
        setDriverETA()
    }
    
    // Sets info in the Uber button. Uses the RideParametersBuilder
    func makeButton(uberButton: RideRequestButton, productGroup: String) {
        guard let pickup = self.pickup else {
            print("Uber Model Button ERROR: pickup location not set")
            return
        }
        
        guard let dropoff = self.dropoff else {
            print("Uber Model Button ERROR: dropoff location not set")
            return
        }

        self.builder.pickupLocation = pickup
        self.builder.dropoffLocation = dropoff
        self.builder.dropoffNickname = "Your Destination"
        self.builder.dropoffAddress = "Your Pickup Location"

        var product: ProductGroup
        // Matches the correct ride kind
        switch productGroup {
            case "uberx": product = .uberX
            case "uberxl": product = .uberXL
            case "uberblack": product = .uberBlack
            case "suv": product = .suv
            default: product = .unknown
        }
        
        // API call to get appropriate product id. Also builds the uber button in the same closure so no race condition and updates the Uber model's variables to reflect the button's variables.
        self.ridesClient.fetchProducts(pickupLocation: pickup) { products, response in
            guard let uber = products.filter({$0.productGroup == product}).first else {
                // Handle error, UberX does not exist at this location
                print("Uber Model Button ERROR: No \(productGroup) rides available in this area")
                return
            }
            self.builder.productID = uber.productID
            self.setProductID(productID: uber.productID)
            uberButton.rideParameters = self.builder.build()
            uberButton.loadRideInformation()
            
            self.updateAll()
        }
    }
}
