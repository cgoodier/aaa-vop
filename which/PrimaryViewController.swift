//
//  arrive-alive-ios
//
//  Created by Cole GOODIER on 2/5/19.
//  Copyright © 2019 Cole GOODIER. All rights reserved.
//
//

import Foundation
import UIKit
import MapKit
import LyftSDK
import UberRides
import Pulley

let lyftModel = LyftModel()
let uberModel = UberModel()

// manages pins/annotations for the mapView
extension PrimaryViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var identifier = "pin"
        if annotation is MKUserLocation {return nil}
        else if annotation.coordinate.latitude ==  fromCoordinate.latitude && annotation.coordinate.longitude == fromCoordinate.longitude {
            identifier = "from"
        }
        else if annotation.coordinate.latitude ==  toCoordinate.latitude && annotation.coordinate.longitude == toCoordinate.longitude {
            identifier = "to"
        }
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            if annotation.coordinate.latitude ==  fromCoordinate.latitude && annotation.coordinate.longitude == fromCoordinate.longitude {
                view.glyphText = "Start"
                view.markerTintColor = UIColor.green
            }
            else if annotation.coordinate.latitude ==  toCoordinate.latitude && annotation.coordinate.longitude == toCoordinate.longitude {
                view.glyphText = "End"
                view.markerTintColor = UIColor.red
            }
        }
        return view
    }
}


class PrimaryViewController: UIViewController, UITextFieldDelegate, locationDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var fromField: UITextField!
    @IBOutlet weak var toField: UITextField!

    var fromCoordinate = CLLocationCoordinate2D()
    var toCoordinate = CLLocationCoordinate2D()
    var fromAddress = ""
    var toAddress = ""

    let locationManager = CLLocationManager()
    let fromFlag = MKPointAnnotation()
    let destFlag = MKPointAnnotation()
    let currentFlag = MKPointAnnotation()
    var fromChanged = false
    var currentFound = false

    let regionRadius: CLLocationDistance = 1500

// map stuff
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }


    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
        
    }

    // initial setup
    override func viewDidLoad() {
        super.viewDidLoad()
        fromField.delegate = self
        toField.delegate = self
        mapView.delegate = self

        // setup for UI elements
        let fromLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 42, height: fromField.frame.height))
        fromLabel.text = " From:"
        fromLabel.font = fromField.font
        fromLabel.textAlignment = fromField.textAlignment
        fromField.leftViewMode = .always
        fromField.leftView = fromLabel

        let toLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 42, height: toField.frame.height))
        toLabel.text = " To:"
        toLabel.font = fromField.font
        toLabel.textAlignment = toField.textAlignment
        toField.leftViewMode = .always
        toField.leftView = toLabel


        let goButton = UIButton.init(frame: CGRect(x: CGFloat(toField.frame.size.width - 42), y: 0, width: 42, height: toField.frame.height - 2))
        goButton.addTarget(self, action: #selector(self.goPress), for: .touchUpInside)
        goButton.backgroundColor = UIColor.blue
        goButton.setTitle("Go", for: .normal)
        goButton.layer.cornerRadius = 6.0
        goButton.layer.masksToBounds = true
        toField.rightView = goButton

        self.view.sendSubviewToBack(mapView)

        // center map on user's current location, default: Davis, CA if not available
        var initialLocation = CLLocation(latitude: 38.5449, longitude: -121.7405)
        initialLocation = locationManager.location ?? CLLocation(latitude: 38.5449, longitude: -121.7405)
        fromCoordinate = initialLocation.coordinate
        if initialLocation == CLLocation(latitude: 38.5449, longitude: -121.7405) {
            fromFlag.coordinate = initialLocation.coordinate
            fromField.text = "Davis, CA, United States"
        }
        else {
            fromField.text = "Current Location"
            currentFlag.coordinate = locationManager.location?.coordinate ?? CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0)
            currentFound = true
        }
        centerMapOnLocation(location: initialLocation)

        // initialize pick up locations
        lyftModel.setPickupLocation(pickup: CLLocation(latitude: fromCoordinate.latitude, longitude: fromCoordinate.longitude))
        uberModel.setPickupLocation(pickup: CLLocation(latitude: fromCoordinate.latitude, longitude: fromCoordinate.longitude))
    }


    // override this to open location entry view
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        if textField == fromField {
            performSegue(withIdentifier: "fromSearch", sender: self)
        }
        else if textField == toField {
            performSegue(withIdentifier: "toSearch", sender: self)
        }
    }

    
    // prepare for location entry view controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromSearch" {
            let dest = segue.destination as! LocationEntryViewController
            dest.delegate = self
            dest.writeTo = 0
        }
        else if segue.identifier == "toSearch" {
            let dest = segue.destination as! LocationEntryViewController
            dest.delegate = self
            dest.writeTo = 1
        }
    }

    
    // retrieve location from the location entry view controller
    func passLocation(coordinate: CLLocationCoordinate2D, address: String?, field: Int?) {
        let drawer = self.pulleyViewController?.drawerContentViewController as? InfoSheetViewController
        // from location entered
        if let f = field {
            if f == 0 {
                fromField.text = address
                fromCoordinate = coordinate
                fromFlag.coordinate = coordinate
                mapView.addAnnotation(fromFlag)

                let pickup = CLLocation(latitude: fromCoordinate.latitude, longitude: fromCoordinate.longitude)
                lyftModel.setPickupLocation(pickup: pickup)
                uberModel.setPickupLocation(pickup: pickup)

                // check if there is a destination coordinate to center the map around
                if (toCoordinate.latitude != 0 && toCoordinate.longitude != 0) {
                    mapView.showAnnotations([fromFlag, destFlag], animated: true)
                    mapView.camera.altitude *= 1.4;

                    //lyftModel.updateAll(rideKind: drawer?.lyftRideType ?? "lyft")
                    uberModel.updateAll()
                    drawer?.configureServiceButtons()

                    // delay to let API calls finish
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                        print("Updating From...")
                    })

                }
                // otherwise center around the single from coordinate
                else {
                    centerMapOnLocation(location: CLLocation(latitude: fromCoordinate.latitude, longitude: fromCoordinate.longitude))
                }
                fromChanged = true
            }

            // to location entered
            else if f == 1 {
                toField.text = address
                toCoordinate = coordinate
                toField.rightViewMode = .always
                destFlag.coordinate = coordinate
                mapView.addAnnotation(destFlag)

                // determine whether to center with current location or a new from coordinate
                if fromChanged == false && currentFound == true {
                    mapView.showAnnotations([currentFlag, destFlag], animated: true)
                    mapView.removeAnnotation(currentFlag)
                    mapView.camera.altitude *= 1.4;
                }
                else {
                    mapView.showAnnotations([fromFlag, destFlag], animated: true)
                    mapView.camera.altitude *= 1.4;
                }

                
                let dropoff = CLLocation(latitude: toCoordinate.latitude, longitude: toCoordinate.longitude)
                lyftModel.setDestinationLocation(destination: dropoff)
                uberModel.setDropoffLocation(dropoff: dropoff)
                //lyftModel.updateAll(rideKind: drawer?.lyftRideType ?? "lyft")
                uberModel.updateAll()
                drawer?.configureServiceButtons()
                
                // delay to let API calls finish
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                    print("Updating To...")
                })

            }
        }
    }

    // update drawer view once user has entered locations and pressed go
    @objc func goPress(sender: UIButton!) {
        let drawer = self.pulleyViewController?.drawerContentViewController as? InfoSheetViewController
        drawer?.updateButtons()
    }

}
