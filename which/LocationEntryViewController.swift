//
//  arrive-alive-ios
//
//  Created by Cole GOODIER on 2/5/19.
//  Copyright © 2019 Cole GOODIER. All rights reserved.
//
//

import Foundation
import UIKit
import MapKit

// used to pass a location and its coordinates back to the primary view controller
protocol locationDelegate {
    func passLocation(coordinate: CLLocationCoordinate2D , address: String?, field: Int?)
}

class LocationEntryViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, MKLocalSearchCompleterDelegate {

    var delegate: locationDelegate?
    var coordinateReturn = CLLocationCoordinate2D()
    var addressReturn: String?
    var writeTo: Int?

    var searchCompleter = MKLocalSearchCompleter()
    var suggestions = [MKLocalSearchCompletion]()

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchResultsDisplay: UITableView!

    
    // return to the primary view without returning coordinates
    @IBAction func cancelPress(_ sender: Any) {
        self.searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion:nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchCompleter.delegate = self
        // searchCompleter.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 39.999733, longitude: -98.678503), span: MKCoordinateSpan(latitudeDelta: 13.589921, longitudeDelta: 14.062500))
        searchResultsDisplay.delegate = self
        searchResultsDisplay.dataSource = self
        searchBar.becomeFirstResponder()
    }

    // table view stuff
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggestions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        let cellData = suggestions[indexPath.row]
        cell.textLabel?.text = cellData.title
        cell.detailTextLabel?.text = cellData.subtitle
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let completion = suggestions[indexPath.row]
        let request = MKLocalSearch.Request(completion: completion)
        let search = MKLocalSearch(request: request)
        search.start { (response, error) in
            self.coordinateReturn = ((response?.mapItems[0].placemark.coordinate)!)
            self.addressReturn = (response?.mapItems[0].placemark.title)
            self.delegate?.passLocation(coordinate: self.coordinateReturn, address: self.addressReturn, field: self.writeTo)
            self.searchBar.resignFirstResponder()
            self.dismiss(animated: true, completion:nil)
        }
    }

    // search bar stuff
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchCompleter.queryFragment = searchText
    }

    // search completer stuff
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        suggestions = completer.results
        searchResultsDisplay.reloadData()
    }

}
