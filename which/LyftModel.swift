//
//  arrive-alive-ios
//
//  Created by Cole GOODIER on 2/5/19.
//  Copyright © 2019 Cole GOODIER. All rights reserved.
//

import Foundation
import CoreLocation
import LyftSDK

class LyftModel {
    var pickup: CLLocationCoordinate2D?
    var destination: CLLocationCoordinate2D?

    var rideKind: String?
    var minCost: Decimal?
    var maxCost: Decimal?
    var driverETA: Int?

    init() {
    }

    // Takes a CLLocation and sets the model's pickup variable to the CLLocation's 2D coordinate.
    func setPickupLocation(pickup: CLLocation) {
        self.pickup = Optional.some(pickup.coordinate)
    }

    // Takes a CLLocation and sets the model's destination variable to the CLLocation's 2D coordinate.
    func setDestinationLocation(destination: CLLocation) {
        self.destination = Optional.some(destination.coordinate)
    }

    // Sets the model's rideKind variable to the ride kind specified by the caller.
    func setRideKind(_ rideKind: String) {
        self.rideKind = Optional.some(rideKind)
        print("Lyft Model Ride Type: \(self.rideKind ?? "Lyft Model ERROR: failed to set rideKind")")
    }

    // Sets the model's min and max cost variables to the values retrieved from the api call based on pickup location, dropoff location, and ride kind.
    func setCost() {
        guard let pickup = self.pickup else {
            print("Lyft Model Cost ERROR: pickup could not be found")
            return
        }

        guard let destination = self.destination else {
            print("Lyft Model Cost ERROR: destination could not be found")
            return
        }

        guard let rideKindRawValue = self.rideKind else {
            print("Lyft Model Cost ERROR: rideKind could not be found")
            return
        }

        var rideKind: RideKind

        // Gets the appropriate RideKind based on the string entered self.rideKind and stores it to rideKind.
        switch rideKindRawValue {
            case "lyft": rideKind = .Standard
            case "lyft_line": rideKind = .Line
            case "lyft_plus": rideKind = .Plus
            case "lyft_lux": rideKind = .Lux
            default: rideKind = .Standard
        }

        // API call that gets lyft estimates.
        LyftAPI.costEstimates(from: pickup, to: destination, rideKind: rideKind) { result in
            result.value?.forEach { costEstimate in
                self.minCost = costEstimate.estimate?.minEstimate.amount
                self.maxCost = costEstimate.estimate?.maxEstimate.amount
                
                // Here, we choose to use the minimum cost since that's what we're using for the price comparisons.
                guard let cost = self.minCost else {
                    print("Lyft Model Cost ERROR: could not get minCost")
                    return
                }
                print("Lyft Model Cost for a \(rideKindRawValue) $\(cost)")
            }
        }
    }

    // Sets the driver's ETA to the value retrieved from the api call based on the pickup location and filtered by the chosen ride kind.
    func setDriverETA() {
        guard let pickup = self.pickup else {
            print("Lyft Model ETA ERROR: pickup could not be found")
            return
        }
        
        // API call to get driver ETAs
        LyftAPI.ETAs(to: pickup) { result in
            guard let etas = result.value else {
                print("Lyft Model ETA ERROR: could not get ETAs")
                return
            }
            guard let rideKind = self.rideKind else {
                print("Lyft Model ETA Error: self.rideKind is not set")
                return
            }
            
            // Grabs the top result from the list of etas, which are filtered by the ride kind.
            guard let eta = etas.filter({$0.rideKind.rawValue == rideKind}).first else {
                print("Lyft Model ETA Error: could not get ETA for a \(rideKind)")
                return
            }
            
            // Sets the model's driverETA model to an Optional wrapped eta (seconds)
            self.driverETA = Optional.some(eta.seconds)
            
            // Checks that the driver ETA was set correctly and gets the value to be printed below
            guard let driverETA = self.driverETA else {
                print("Lyft Model ETA ERROR: self.driverETA is not set")
                return
            }
            print("Lyft Model ETA for a \(rideKind): \(driverETA.description) seconds")
        }
    }

    // Updates the model variables
    func updateAll(rideKind: String) {
        self.setRideKind(rideKind)
        self.setCost()
        self.setDriverETA()
    }

    // Gives the lyft button the info it needs through the configure method and then calls updateAll so the model variables will reflect what the button shows.
    func makeButton(lyftButton: LyftButton, rideKind: String) {
        guard let pickup = self.pickup else {
            print("Lyft Model Button ERROR: self.pickup is not set")
            return
        }
        
        guard let destination = self.destination else {
            print("Lyft Model Button ERROR: self.destination is not set")
            return
        }
        
        var ride: RideKind
        
        // Gets the appropriate RideKind based on the string entered self.rideKind and stores it to rideKind.
        switch rideKind {
            case "lyft": ride = .Standard
            case "lyft_line": ride = .Line
            case "lyft_plus": ride = .Plus
            case "lyft_lux": ride = .Lux
            default: ride = .Standard
        }

        lyftButton.style = .hotPink
        lyftButton.configure(rideKind: ride, pickup: pickup, destination: destination) // Where the button gets its info
        self.updateAll(rideKind: rideKind)
    }

}
