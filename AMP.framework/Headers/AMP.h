//
//  AMP.h
//

#import <Foundation/Foundation.h>

//! Project version number for temp.
FOUNDATION_EXPORT double ampVersionNumber;

//! Project version string for temp.
FOUNDATION_EXPORT const unsigned char ampVersionString[];

typedef NS_ENUM(NSInteger, AmpEnvironment) {
    NGA_STAGE = 0,
    NGA_IGAPP = 1,
    NGA_PRODUCTION = 2,
    CTI_DMZ = 3
};

typedef NS_ENUM(NSInteger, AmpLoginRequirement) {
    ONCE = 0, // Default
    NEVER = 1,
    ALWAYS
};

@interface AMP : NSObject

+(void)start;
+(void)startWithCompletion:(void (^)(void))completionBlock;
+(void)startWithEnvironment:(AmpEnvironment)environment signature:(NSString*)signature;
+(void)startWithEnvironment:(AmpEnvironment)environment signature:(NSString*)signature completion:(void (^)(void))completionBlock;
+(void)setLoginRequirement:(AmpLoginRequirement)loginRequirement;
+(void)setDuration:(int)duration;
+(void)setUsagesBeforeRatingsPrompt:(int)usages;

+(NSString*)getDeviceId;
+(NSString*)getLoginId;
+(void)checkProvisioningProfile;
+(BOOL)isNetworkAvailable;

@end
